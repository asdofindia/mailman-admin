package `in`.learnlearn.mailmanadmin.di

import `in`.learnlearn.mailmanadmin.InternetDataRepository
import `in`.learnlearn.mailmanadmin.MailmanDataRepository
import `in`.learnlearn.mailmanadmin.api.DefaultHTTPClient
import `in`.learnlearn.mailmanadmin.api.HTTPClient
import `in`.learnlearn.mailmanadmin.api.MapCookieStore
import `in`.learnlearn.mailmanadmin.preferences.Configuration
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {
    @Binds
    @Singleton
    abstract fun bindHttpClient(defaultHTTPClient: DefaultHTTPClient): HTTPClient

    @Binds
    @Singleton
    abstract fun bindDataRepository(internetDataRepository: InternetDataRepository): MailmanDataRepository
}

@Module
@InstallIn(SingletonComponent::class)
object Instances {
    @Provides
    @Singleton
    fun provideConfiguration(@ApplicationContext context: Context): Configuration {
        return Configuration(context)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient() =
        OkHttpClient.Builder().cookieJar(MapCookieStore()).build()

}