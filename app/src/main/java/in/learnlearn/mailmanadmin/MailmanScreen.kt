package `in`.learnlearn.mailmanadmin

import `in`.learnlearn.mailmanadmin.api.Mail
import `in`.learnlearn.mailmanadmin.api.MailmanState
import android.app.Activity
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

@Preview(showBackground = true)
@Composable
fun MailmanScreen(
    mailmanViewModel: MailmanViewModel = hiltViewModel()
) {
    val mailmanState by mailmanViewModel.state.collectAsState()
    val activity = (LocalContext.current as? Activity)
    when (val presentState = mailmanState) {
        MailmanState.Connecting -> StatusMessage("Connecting...")
        is MailmanState.Error -> StatusMessage("Error: ${presentState.message}. Are you connected to internet?")
        MailmanState.LoggingIn -> StatusMessage("Logging in...")
        MailmanState.PasswordWrong -> StatusMessage("Password is wrong")
        MailmanState.PendingConfiguration -> StatusMessage("Configuration not supplied")
        is MailmanState.PendingMails -> PendingMailsDisplay(presentState.mails) { command ->
            when (command) {
                "discard" -> mailmanViewModel.discardMails()
                "refresh" -> mailmanViewModel.refresh()
                "exit" -> {
                    Toast.makeText(activity, "Bye, bye!", Toast.LENGTH_SHORT).show()
                    activity?.finish()
                }
            }
        }
        MailmanState.Processing -> StatusMessage("Processing...")
        MailmanState.Discarding -> StatusMessage("Discarding...")
    }
}

@Composable
fun StatusMessage(text: String) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = text, style = MaterialTheme.typography.h5)
    }
}

@Composable
fun EmptyList(commander: (String) -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {
        Text("No pending mails. Check back later", style = MaterialTheme.typography.h5)
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = { commander("refresh") }) {
            Text("Refresh")
        }
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = { commander("exit") }) {
            Text("Exit")
        }
    }
}

@Composable
fun PendingMailsDisplay(
    mails: List<Mail>,
    commander: (String) -> Unit
) {
    if (mails.isEmpty()) {
        EmptyList(commander)
    } else {
        LazyColumn(horizontalAlignment = Alignment.CenterHorizontally) {
            items(mails) { mail ->
                Card(
                    shape = RoundedCornerShape(3.dp),
                    backgroundColor = MaterialTheme.colors.background,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    Column {
                        Text(
                            text = mail.from,
                            modifier = Modifier.padding(16.dp, 4.dp),
                            style = MaterialTheme.typography.subtitle1
                        )
                        Spacer(modifier = Modifier.requiredHeight(8.dp))
                        Text(
                            text = mail.subject,
                            modifier = Modifier.padding(16.dp, 0.dp),
                            style = MaterialTheme.typography.subtitle2
                        )

                    }
                }
            }
            item {
                DiscardButton(commander)
            }
        }
    }
}

@Composable
fun DiscardButton(commander: (String) -> Unit) {
    Button(onClick = { commander("discard") }) {
        Text("Discard")
    }
}

