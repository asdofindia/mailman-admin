package `in`.learnlearn.mailmanadmin

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

@Composable
fun MailmanNavigation(
    loginViewModel: LoginViewModel = hiltViewModel()
) {
    val navController = rememberNavController()
    val startDestination =
        if (loginViewModel.instance.isNullOrBlank() || loginViewModel.password.isNullOrBlank()) {
            "login"
        } else {
            "mails"
        }


    NavHost(navController = navController, startDestination = startDestination) {
        composable(route = "login") {
            LoginScreen { instance, password ->
                loginViewModel.setInstance(instance)
                loginViewModel.setPassword(password)
                navController.navigate("mails")
            }
        }
        composable(route = "mails") { MailmanScreen() }
    }
}

