package `in`.learnlearn.mailmanadmin

import `in`.learnlearn.mailmanadmin.preferences.Configuration
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val configuration: Configuration
) : ViewModel() {
    val instance = configuration.instance
    val password = configuration.password
    fun setInstance(instance: String?) {
        configuration.instance = instance
    }

    fun setPassword(password: String?) {
        configuration.password = password
    }
}
