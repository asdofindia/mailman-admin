package `in`.learnlearn.mailmanadmin.api

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import javax.inject.Inject

data class Mail(val from: String, val subject: String, val body: String)

sealed class MailmanState {
    object PendingConfiguration : MailmanState()
    object Connecting : MailmanState()
    object LoggingIn : MailmanState()
    object PasswordWrong : MailmanState()
    object Processing : MailmanState()
    object Discarding : MailmanState()
    data class PendingMails(val mails: List<Mail>) : MailmanState()
    data class Error(val message: String) : MailmanState()
}

sealed class MailmanWebResponse {
    object LoginRequired : MailmanWebResponse()
    object PendingMails : MailmanWebResponse()
    object NonePending : MailmanWebResponse()
}

class MailmanWeb @Inject constructor(
    private val httpClient: HTTPClient
) {
    private val _state: MutableStateFlow<MailmanState> =
        MutableStateFlow(MailmanState.PendingConfiguration)
    val state = _state.asStateFlow()

    private lateinit var configuration: MailmanInstanceConfiguration

    private lateinit var lastDoc: Document

    suspend fun setConfiguration(config: MailmanInstanceConfiguration) {
        configuration = config
        refresh()
    }

    private fun processResponse(responseText: String): MailmanWebResponse {
        lastDoc = Jsoup.parse(responseText)
        val loginElement = lastDoc.select("input[name=\"adminpw\"]")
        if (loginElement.size > 0) {
            return MailmanWebResponse.LoginRequired
        }
        val nonePending = lastDoc.text().contains("There are no pending requests")
        return if (nonePending) {
            MailmanWebResponse.NonePending
        } else {
            MailmanWebResponse.PendingMails
        }
    }

    suspend fun refresh() {
        _state.value = MailmanState.Connecting
        val response = httpClient.get(configuration.baseUrl)
        updateState(response, false)
    }

    private suspend fun updateState(response: WebResult, loginTried: Boolean = false) {
        when (response) {
            is WebResult.Error -> _state.value = MailmanState.Error(response.error)
            is WebResult.Success -> {
                val mailmanState = processResponse(response.body)
                when (mailmanState) {
                    MailmanWebResponse.LoginRequired ->
                        if (loginTried) {
                            wrongPassword()
                        } else {
                            login()
                        }
                    MailmanWebResponse.PendingMails -> processPendingMails()
                    MailmanWebResponse.NonePending -> noPendingMails()
                }
            }
        }

    }

    private fun noPendingMails() {
        _state.value = MailmanState.PendingMails(listOf())
    }

    private suspend fun login() {
        _state.value = MailmanState.LoggingIn
        val responseText = httpClient.post(
            configuration.baseUrl,
            mapOf("admlogin" to "Let me in...", "adminpw" to configuration.password)
        )
        updateState(responseText, loginTried = true)
    }

    private fun processPendingMails() {
        _state.value = MailmanState.Processing
        val links = lastDoc.select("a[href*=\"msgid\"]")
        val mails = mutableListOf<Mail>()
        for (link in links) {
            val subject = link.parent()?.nextElementSibling()?.nextElementSibling()
            val parents = link.parents()
            val from = parents.let {
                var tbodiesFound = 0
                var requiredParent: Element? = null
                for (parent in it) {
                    if (parent.normalName() == "tbody") {
                        tbodiesFound += 1
                        if (tbodiesFound == 3) {
                            requiredParent = parent
                            break
                        }
                    }
                }
                requiredParent?.select("tr")?.get(0)?.text()?.drop(5) ?: "Unknown sender"
            }
            mails.add(Mail(from, subject?.text() ?: "Couldn't find subject", ""))
        }
        _state.value = MailmanState.PendingMails(mails)
    }

    private fun wrongPassword() {
        _state.value = MailmanState.PasswordWrong
    }

    suspend fun discardAllMails() {
        _state.value = MailmanState.Discarding
        val data = mutableMapOf(
            "discardalldefersp" to "0",
        )
        for (input in lastDoc.select("input")) {
            if (input.hasAttr("CHECKED") || input.attr("type") == "hidden") {
                data += input.attr("name") to input.attr("value")
            }
        }
        val response = httpClient.post(
            configuration.baseUrl,
            data
        )
        updateState(response, false)
    }
}
