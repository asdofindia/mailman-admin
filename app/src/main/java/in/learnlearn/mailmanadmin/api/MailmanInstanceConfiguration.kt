package `in`.learnlearn.mailmanadmin.api

data class MailmanInstanceConfiguration(val baseUrl: String, val password: String)
