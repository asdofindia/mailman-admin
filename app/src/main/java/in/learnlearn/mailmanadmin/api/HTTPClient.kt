package `in`.learnlearn.mailmanadmin.api

import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrl
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

interface HTTPClient {
    suspend fun get(url: String): WebResult
    suspend fun post(baseUrl: String, params: Map<String, String>): WebResult
}

sealed class WebResult {
    data class Success(val body: String) : WebResult()
    data class Error(val error: String) : WebResult()
}

class DefaultHTTPClient @Inject constructor(private val okHttpClient: OkHttpClient) : HTTPClient {

    override suspend fun get(url: String): WebResult {
        val request = Request(url.toHttpUrl())
        return executeWithOkHttp(request)
    }

    override suspend fun post(baseUrl: String, params: Map<String, String>): WebResult {
        val formBody = getFormBody(params)
        val request = Request(url = baseUrl.toHttpUrl(), body = formBody)
        return executeWithOkHttp(request)
    }

    private fun getFormBody(params: Map<String, String>) = FormBody.Builder()
        .also { bodyBuilder ->
            for ((key, value) in params) {
                bodyBuilder.add(key, value)
            }
        }.build()


    private suspend fun executeWithOkHttp(request: Request): WebResult =
        suspendCoroutine { continuation ->
            runCatching {
                okHttpClient.newCall(request).execute()
                    .use { response -> continuation.resume(WebResult.Success(response.body.string())) }
            }.onFailure {
                continuation.resume(WebResult.Error(it.localizedMessage ?: it.toString()))
            }
        }

}

class MapCookieStore : CookieJar {
    private val store = hashMapOf<String, List<Cookie>>()
    override fun loadForRequest(url: HttpUrl): List<Cookie> {
        return store[url.host] ?: emptyList()
    }

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        store[url.host] = combineCookies(store[url.host] ?: emptyList(), cookies)
    }
}

fun combineCookies(old: List<Cookie>, new: List<Cookie>): List<Cookie> {
    var hashed = addToHash(mutableMapOf(), old)
    hashed = addToHash(hashed, new)
    return mapToList(hashed)
}

fun mapToList(hashed: MutableMap<String, Cookie>): List<Cookie> {
    return hashed.values.toList()
}

fun addToHash(map: MutableMap<String, Cookie>, cookies: List<Cookie>): MutableMap<String, Cookie> {
    for (cookie in cookies) {
        map[cookie.name] = cookie
    }
    return map
}

