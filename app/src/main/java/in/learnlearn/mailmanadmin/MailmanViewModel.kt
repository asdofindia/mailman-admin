package `in`.learnlearn.mailmanadmin

import `in`.learnlearn.mailmanadmin.api.MailmanInstanceConfiguration
import `in`.learnlearn.mailmanadmin.preferences.Configuration
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class MailmanViewModel @Inject constructor(
    private val repository: MailmanDataRepository,
    configuration: Configuration
) : ViewModel() {
    val state = repository.getState()

    init {

        val mailmanInstanceConfiguration = MailmanInstanceConfiguration(
            configuration.instance!!,
            configuration.password!!
        )
        viewModelScope.launch(Dispatchers.IO) {
            repository.setConfiguration(mailmanInstanceConfiguration)
        }
    }

    fun discardMails() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.discardAllMails()
        }
    }

    fun refresh() {
        viewModelScope.launch(Dispatchers.IO) { repository.refresh() }
    }

}