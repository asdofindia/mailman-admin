package `in`.learnlearn.mailmanadmin.preferences

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class Configuration @Inject constructor(@ApplicationContext context: Context) {
    private val prefs = context.getSharedPreferences("login", Context.MODE_PRIVATE)
    var instance: String?
        get() = getFromPrefs("instance")
        set(instance) {
            instance?.let { putInPrefs("instance", it) } ?: removeFromPref("instance")
        }

    var password: String?
        get() = getFromPrefs("password")
        set(password) {
            password?.let { putInPrefs("password", it) } ?: removeFromPref("password")
        }

    private fun getFromPrefs(key: String): String? = prefs.getString(key, null)

    private fun putInPrefs(key: String?, value: String) {
        with(prefs.edit()) {
            putString(key, value)
            apply()
        }
    }

    private fun removeFromPref(key: String) {
        with(prefs.edit()) {
            remove(key)
            apply()
        }
    }
}