package `in`.learnlearn.mailmanadmin

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

@Composable
fun LoginScreen(
    loginViewModel: LoginViewModel = hiltViewModel(),
    onLoggingIn: (String?, String?) -> Unit
) {
    var instance by remember { mutableStateOf(loginViewModel.instance) }
    var password by remember { mutableStateOf(loginViewModel.password) }
    Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center) {
        TextField(
            value = instance ?: "",
            onValueChange = { instance = it },
            label = { Text("Instance") },
            placeholder = { Text("http://lists.smc.org.in/admindb.cgi/discuss-smc.org.in") },
            modifier = Modifier.fillMaxWidth()
        )
        TextField(
            value = password ?: "",
            onValueChange = { password = it },
            label = { Text("Password") },
            modifier = Modifier.fillMaxWidth(),
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = { onLoggingIn(instance, password) }) {
            Text("Save & Login")
        }
    }
}