package `in`.learnlearn.mailmanadmin

import `in`.learnlearn.mailmanadmin.api.MailmanInstanceConfiguration
import `in`.learnlearn.mailmanadmin.api.MailmanState
import `in`.learnlearn.mailmanadmin.api.MailmanWeb
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject


interface MailmanDataRepository {
    suspend fun setConfiguration(mailmanInstanceConfiguration: MailmanInstanceConfiguration)
    fun getState(): StateFlow<MailmanState>
    suspend fun discardAllMails()
    suspend fun refresh()
}

class InternetDataRepository @Inject constructor(
    private val mailmanWeb: MailmanWeb
) : MailmanDataRepository {

    override fun getState(): StateFlow<MailmanState> {
        return mailmanWeb.state
    }

    override suspend fun discardAllMails() {
        mailmanWeb.discardAllMails()
    }

    override suspend fun refresh() {
        mailmanWeb.refresh()
    }

    override suspend fun setConfiguration(mailmanInstanceConfiguration: MailmanInstanceConfiguration) {
        mailmanWeb.setConfiguration(mailmanInstanceConfiguration)
    }

}